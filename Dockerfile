FROM hseeberger/scala-sbt:8u181_2.12.8_1.2.8

WORKDIR /usr/src/app

COPY . /usr/src/app

RUN sbt "; set assemblyOutputPath in assembly := file(\"./app.jar\"); assembly"

FROM gcr.io/distroless/java

COPY --from=0 /usr/src/app/app.jar /

EXPOSE 8080

CMD ["-server", "-XX:+UseG1GC", "/app.jar"]
